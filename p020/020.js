/**
 * n! means n × (n − 1) × ... × 3 × 2 × 1

For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.

Find the sum of the digits in the number 100!
 */

const NUM = 100;
function factorial(num) {
  let fact = 1;
  for (let i = num; i > 0; i--) {
    fact *= i;
  }
  return fact.toString();
}

function getSumFactorial() {
  let fact = factorial(NUM);
  fact = fact.substr(0, 1) + fact.substr(2);
  fact = fact.substr(0, 10);
  let sum = 0;
  for (let i = 0; i < fact.length; i++) {
    sum += fact.charAt(i);
  }
  return sum;
}

console.log(getSumFactorial());
