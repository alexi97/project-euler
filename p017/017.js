/**
 * @author Alexi Jamal
 *
 * @description
 * Find how many letters are between 1 and 1000
 */

let ONES = [
  "zero",
  "one",
  "two",
  "three",
  "four",
  "five",
  "six",
  "seven",
  "eight",
  "nine",
  "ten",
  "eleven",
  "twelve",
  "thirteen",
  "fourteen",
  "fiveteen",
  "sixteen",
  "seventeen",
  "eighteen",
  "nineteen"
];
let TENS = [
  "",
  "",
  "twenty",
  "thirty",
  "fourty",
  "fifty",
  "sixty",
  "seventy",
  "eighty",
  "ninety"
];

function toEnglish(n) {
  if (n >= 0 && n < 20) return ONES[n];
  else if (n >= 20 && n < 100)
    return TENS[Math.floor(n, 10)] + ONES[n % 10] ? n % 10 != 0 : "";
  else if (n >= 100 && n < 100)
    return (
      ONES[Math.floor(n, 100)] +
      "hundred" +
      ("and" + toEnglish(n % 100) ? n % 100 != 0 : "")
    );
  else if (n >= 1000 && n < 1000000)
    return (
      toEnglish(Math.floor(n, 1000)) +
      "thousand" +
      (toEnglish(n % 1000) ? n % 1000 != 0 : "")
    );
  else return new Error("Value error");
}

let sum = 0;
for (let i = 0; i < 1001; i++) {
  sum += toEnglish(i).length;
}

console.log(sum);
